//Bootstrap
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

// フォームの値を取得する方法下記参照↓
// https://www.atmarkit.co.jp/ait/articles/1609/01/news072.html

// 種族値の配列
var basestatsArr = ["hbs", "abs", "bbs", "cbs", "dbs", "sbs"];
// 実数値の配列
var actualvalueArr = ["hav", "aav", "bav", "cav", "dav", "sav"]
// 努力値増減の配列
var evArr = ["hev", "aev", "bev", "cev", "dev", "sev"];
// 個体値の配列
var ivArr = ["hiv", "aiv", "biv", "civ", "div", "siv"];
// 性格の配列
var natureArr = ["hn", "an", "bn", "cn", "dn", "sn"];
// 計算式の配列
var formulaArr = ["hf", "af", "bf", "cf", "df", "sf"];

var pokeSearch = document.querySelector("#pokelist");

// 入力するたびに配列がリセットされるので関数の外で宣言
var pokeSuggest = [];

// 検索予測
function search() {
    // 入力された文字を配列に格納
    // const input = document.form.elements["pokemon"].value.split("");
    const input = document.querySelector(".pokemonList").value.split("");
    pokemonbook.forEach(element => {
        var pokeSplit = element[0].split("");
        // console.log(pokeSplit);
        for (p = 0; p < pokeSplit.length; p++) {
            if (input[p] == pokeSplit[p]) {
                const option = document.createElement("option");
                var pokeArr = element.find(pokeList => {
                    return input[p] == pokeList[0];
                });
                option.value = pokeArr;
                // 配列に存在するポケモンは予測変換に出さない
                if (pokeSuggest.indexOf(pokeArr) == -1) {
                    pokeSearch.appendChild(option);
                    pokeSuggest.push(pokeArr);
                }
            }
        }
    });
}

const input = document.querySelector(".pokemonList").value;



// レベルを生成
const levelParent = document.querySelector(".levelParent");
for (let l = 100; l >= 0; l--) {
    const element = document.createElement('option');
    element.classList.add("level");
    // レベル50をデフォルトに設定
    element.value = l;
    element.textContent = l;
    // optionタグを51まで作成したら50番目のタグにselectedを付与
    if (l == 49) {
        levelParent.options[50].selected = true;
    }
    levelParent.appendChild(element);
}

// レベルボタン
function levelButton(level) {
    if (level == 50) {
        const level50 = document.querySelector(".level50").value;
        const levelParent = document.querySelector(".levelParent");
        levelParent.value = level50;
    }
    if (level == 100) {
        console.log(level);
        const level100 = document.querySelector(".level100").value;
        const levelParent = document.querySelector(".levelParent");
        levelParent.value = level100;
    }
}


// 個体値を生成
const parents = document.querySelectorAll(".iv");
for (let i = 0; i < parents.length; i++) {
    for (let v = 31; v >= 0; v--) {
        const elements = document.createElement('option');
        elements.classList.add("ivChild");
        elements.textContent = v;
        parents[i].appendChild(elements);
    }
}
// 性格一覧を表示
const natureLi = document.querySelector("#natureLi");
natureList.sort();
for (let i = 0; i < natureList.length; i++) {
    const elements = document.createElement('option');
    elements.textContent = natureList[i];
    console.log(elements);
    natureLi.appendChild(elements);
}

// リセットボタン
function reset() {
    document.form.reset();
}

// 種族値を反映
function pokemons() {
    for (i = 0; i < pokemonbook.length; i++) {
        // 名前が一致したら種族値をセット
        if (document.form.elements["pokemon"].value == pokemonbook[i][0]) {
            document.form.elements[basestatsArr[0]].value = pokemonbook[i][1];
            document.form.elements[basestatsArr[1]].value = pokemonbook[i][2];
            document.form.elements[basestatsArr[2]].value = pokemonbook[i][3];
            document.form.elements[basestatsArr[3]].value = pokemonbook[i][4];
            document.form.elements[basestatsArr[4]].value = pokemonbook[i][5];
            document.form.elements[basestatsArr[5]].value = pokemonbook[i][6];
        }
    }
}

// 努力値増加
function updown(num, num2) {
    let upDown = parseInt(document.form.elements[evArr[num]].value, 10);
    console.log(upDown);
    // 第1引数が0の場合は増加
    if (num2 == 0 && upDown == 0) {
        let add = upDown += 4;
        document.form.elements[evArr[num]].value = upDown;
    } else {
        if (num2 == 0 && upDown !== 0 && upDown >= 4 && upDown <= 244) {
            let add = upDown += 8;
            document.form.elements[evArr[num]].value = upDown;
        }
    }
    // 第2引数が1の場合は減少
    if (num2 == 1 && upDown == 4) {
        let add = upDown -= 4;
        document.form.elements[evArr[num]].value = upDown;
    } else {
        if (num2 == 1 && upDown >= 4) {
            let add = upDown -= 8;
            document.form.elements[evArr[num]].value = upDown;
        }
    }
}

// 努力値の最大最小
function maxmin(max, min) {
    // 第1引数が0の場合は増加
    maxE = parseInt(document.form.elements[evArr[max]].value, 10);
    // 数値がエラーだったら赤字を解除
    if(document.form.elements[evArr[max]].className == "tableInput ev form-control red") {
        document.form.elements[evArr[max]].classList.remove("red");
    }
    document.form.elements[evArr[max]].value = 252;
    // 第2引数が1の場合は減少
    if (min == 1) {
        document.form.elements[evArr[max]].value = 0;
    }
}

// HPの種族値から実数値を計算
function hcalc(bs, iv, ev, level) {
    if (isNaN(bs) == false && isNaN(ev) == false) {
        // 実数値 = ((種族値 * 2 + 個体値 + 努力値 / 4) * レベル / 100) + 10 + レベル
        const av = Math.floor(((bs * 2 + iv + ev / 4) * level / 100) + 10 + level);
        const formula = `実数値 = ((${bs} × 2 + ${iv} + ${ev} ÷ 4) × ${level} ÷ 100) + 10 + ${level}`;
        return [av, formula];
    }
}

// ABCDSの種族値から実数値を計算
function abcdsCalcTest(bs, iv, ev, level, nature, num) {
    if (isNaN(bs) == false && isNaN(ev) == false) {
        //実数値 = (((種族値 × 2 + 個体値 + 努力値 / 4) × レベル / 100) + 5) × 性格補正(0.9、1.0、1.1)
        const av = Math.floor((((bs * 2 + iv + ev / 4) * level / 100) + 5));
        console.log(av);
        // nCalc.jsを参照
        const avNature = Math.floor(av * nCalc(nature, num));
        const formula = `実数値 = (((${bs} × 2 + ${iv} + ${ev} ÷ 4) × ${level} ÷ 100) + 5) × ${nature}`;
        return [avNature, formula];
    }
}

// 種族値から実数値を計算
function calc(num, num2) {
    const hbs = parseInt(document.form.elements[basestatsArr[num]].value, 10);//種族値
    const hiv = parseInt(document.form.elements[ivArr[num]].value, 10);//個体値
    const hev = parseInt(document.form.elements[evArr[num]].value, 10);//努力値
    const level = parseInt(document.form.elements["level"].value, 10);//レベル
    // const nature = Number(document.form.elements[natureArr[num]].value);//性格
    const nature = document.querySelector("#natureLi").value; //性格
    console.log(level);
    // 数値が入力されたらエラーを解除
    const hVali = document.form.elements[basestatsArr[num]];
    if (hVali.className == "tableInput form-control red") {
        hVali.classList.remove("red");
    }

    if (num == 0) {
        const calc = hcalc(hbs, hiv, hev, level);
        // HPの実数値を表示
        document.form.elements[actualvalueArr[num]].value = calc[0];
        // // HPの計算式を表示
        // document.form.elements[formulaArr[num]].value = calc[1];
    } else {
        if (num2 = 1) {
            const calcNoth = abcdsCalcTest(hbs, hiv, hev, level, nature, num);
            // ABCDSの実数値を表示
            console.log(num);
            document.form.elements[actualvalueArr[num]].value = calcNoth[0];
            // ABCDSの計算式を表示
            // document.form.elements[formulaArr[num]].value = calcNoth[1];
        }
    }
}

// HPの実数値から種族値を計算
// function bshcalc(hav, iv, ev, level) {
//     if(isNaN(hav) == false && isNaN(ev) == false) {
//         //種族値 = 実数値 - ((個体値 + 努力値 / 4) * レベル / 100) -10 - レベル
//         const bs = Math.ceil((hav - (iv + ev / 4) * level / 100) -10 - level);
//         const formula = `種族値 = ((${iv} + ${ev} / 4) * ${level} / 100) -10 - ${level}`;
//         return [bs, formula];
//     }
// }

// ABCDSの実数値から種族値を計算
// function bsabcdscalc(bs, iv ,ev, level, nature) {
//     if(isNaN(bs) == false && isNaN(ev) == false) {
//         //種族値 = 実数値 / 性格　- ((個体値 + 努力値 / 4) * レベル / 100) - 5
//         const av = Math.ceil(bs / nature - ((iv + ev / 4) * level / 100) - 5);
//         // const test = Math.ceil(169 / 1.1 - ((31 + 252 / 4) * 50 / 100) - 5);
//         const formula = `種族値 = ${bs} / ${nature} - ((${iv} + ${ev} / 4) * ${level} / 100) - 5`;
//         return [av, formula];
//     }
// }

// 実数値から種族値を計算
// function bscalc(num, num2) {
//     const hav = parseInt(document.form.elements[actualvalueArr[num]].value, 10);//実数値
//     const hiv = parseInt(document.form.elements[ivArr[num]].value, 10);//個体値
//     const hev = parseInt(document.form.elements[evArr[num]].value, 10);//努力値
//     const level =　parseInt(document.form.elements["level"].value, 10);//レベル
//     const nature = Number(document.form.elements[natureArr[num]].value);//性格

//     if(num == 0) {
//         const calc = bshcalc(hav, hiv, hev, level);
//         // HPの実数値を表示
//         document.form.elements[basestatsArr[num]].value = calc[0];
//         // // HPの計算式を表示
//         document.form.elements[formulaArr[num]].value = calc[1];
//     }else {
//         if(num2 = 1) {
//             const calcNoth = bsabcdscalc(hav, hiv, hev, level, nature);
//             // ABCDSの実数値を表示
//             document.form.elements[basestatsArr[num]].value = calcNoth[0];
//             // ABCDSの計算式を表示
//             document.form.elements[formulaArr[num]].value = calcNoth[1];
//         }
//     }
// }

// HPの実数値から努力値を計算
function evhcalc(hav, hbs, hiv, level) {
    if (isNaN(hav) == false && isNaN(hbs) == false) {
        // HP努力値 = (((実数値 - レベル - 10) * 100 / レベル) - 種族値 * 2 - 個体値) * 4
        const subtraction = hav - level - 10;
        //小数点切り上げ 
        const levelCeil = Math.ceil(subtraction * 100 / level);
        const ev = (levelCeil - hbs * 2 - hiv) * 4;
        console.log(ev);
        return ev;
    }
}


function abcdsCalc(hav, nature, hbs, hiv, level, num) {
    if (isNaN(hav) == false && isNaN(hbs) == false) {
        //努力値 = ((((実数値 / 性格）- 5) * 100 / レベル) - 種族値 * 2 - 個体値) * 4
        // nCalc.js参照
        const natureCalc = nCalc(nature, num);
        console.log(natureCalc);
        //小数点切り上げ
        const natureCeil = Math.ceil((hav / natureCalc) - 5);
        console.log(natureCeil);
        console.log(nature);
        //小数点切り上げ 
        const levelCeil = Math.ceil(natureCeil * 100 / level);
        console.log(levelCeil);
        const ev = (levelCeil - hbs * 2 - hiv) * 4;
        return ev;
    }
}

// 実数値から努力値を計算
function evCalc(num, num2) {
    const hav = parseInt(document.form.elements[actualvalueArr[num]].value, 10);//実数値
    const hbs = parseInt(document.form.elements[basestatsArr[num]].value, 10);//種族値
    const hiv = parseInt(document.form.elements[ivArr[num]].value, 10);//個体値
    // const nature = Number(document.form.elements[natureArr[num]].value);//性格
    const nature = document.querySelector("#natureLi").value; //性格
    const level = parseInt(document.form.elements["level"].value, 10);//レベル

    // 種族値がセットされてない場合赤色に変化
    if (isNaN(hbs) == true) {
        document.form.elements[basestatsArr[num]].classList.add("red");
    }

    if (num == 0) {
        const calc = evhcalc(hav, hbs, hiv, level);
        // 努力値を表示
        if (calc !== undefined) {
        // 努力値がマイナスになる場合は努力値を0にする03/09kojima
            if(calc < 0) {
                document.form.elements[evArr[num]].value = 0;
            } else {
                document.form.elements[evArr[num]].value = calc;
            }
            // 努力値の閾値を超えたらエラーを表示
            if (calc > 252) {
                document.form.elements[evArr[num]].classList.add("red");
            } else {
                if (calc <= 252) {
                    document.form.elements[evArr[num]].classList.remove("red");
                }
            }
        }
    } else {
        if (num2 == 1) {
            const calcNoth = abcdsCalc(hav, nature, hbs, hiv, level, num);
            //努力値を表示
            if (calcNoth !== undefined) {
                console.log(calcNoth);
                // 努力値がマイナスになる場合は努力値を0にする03/09kojima
                if(calcNoth < 0) {
                    document.form.elements[evArr[num]].value = 0;
                } else {
                    document.form.elements[evArr[num]].value = calcNoth;
                }
                // 努力値の閾値を超えたらエラーを表示
                if (calcNoth > 252) {
                    document.form.elements[evArr[num]].classList.add("red");
                } else {
                    if (calcNoth <= 252) {
                        document.form.elements[evArr[num]].classList.remove("red");
                    }
                }
            }
        }
    }
}

    // 計算式の表示非表示
    // function display() {
    //     const disp = document.querySelectorAll(".formula");
    //     const off = document.querySelectorAll(".off");
    //     const notDisp = document.querySelector("#onOff");

    //     if(notDisp.value == "計算式を確認する") {
    //         notDisp.value = "計算式を非表示にする";
    //         for(i = 0; i < disp.length; i++) {
    //             disp[i].classList.remove("formula");
    //         }
    //     }
    //     else {
    //         if(notDisp.value == "計算式を非表示にする") {
    //             notDisp.value = "計算式を確認する";
    //             for(i = 0; i < off.length; i++) {
    //                 off[i].classList.add('formula');
    //             }
    //         }
    //     }
    // }
